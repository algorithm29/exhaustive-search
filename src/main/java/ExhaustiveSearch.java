

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ASUS
 */
public class ExhaustiveSearch {
     public int maxProfit(int[] prices) {
        int minPriceSoFar = Integer.MAX_VALUE, maxProfitSoFar = 0;
        
        for (int i = 0; i < prices.length; i++) {
            if (minPriceSoFar > prices[i]) {
                minPriceSoFar = prices[i];
            } else {
                maxProfitSoFar = Math.max(maxProfitSoFar, prices[i] - minPriceSoFar);
            }
        }
        
        return maxProfitSoFar;
    }
     
     public static void main(String[] args) {
       
        ExhaustiveSearch Ehs = new ExhaustiveSearch();
        int[] prices = {10,18,16,17,13,12};
        
        System.out.println(Ehs.maxProfit(prices));
    
    }



}
